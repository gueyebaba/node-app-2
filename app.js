const express = require('express');
const app = express();
app.set('view engine', 'ejs');
app.set('views', 'views');
const path = require('path');
const bodyParser = require('body-parser');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const rootDir = require('./util/path');

app.use(bodyParser.urlencoded({ extended: false }));
app.use('/admin', adminRoutes.router);
app.use(shopRoutes);
app.use(express.static(path.join(rootDir, 'public')));

app.use((req, res, next) => {
    res.status(404).render('404', {pageTitle: '404 - Page Not Found'});
});

app.listen(3000); 